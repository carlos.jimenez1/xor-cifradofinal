package cifrado;

import javax.swing.JOptionPane;

/**
 *
 * @author Carlos Alberto
 */
public class Principal {

    public static void main(String[] args) {
        Encriptar e = new Encriptar();
        Desencriptar d = new Desencriptar();

        String mensaje = JOptionPane.showInputDialog(null, "Ingrese el mensaje a enviar");
        String clave = JOptionPane.showInputDialog(null, "Ingrese el la clave para cifrar");

        if (clave.length() == 1) {
            JOptionPane.showMessageDialog(null, "El mensaje original es: " + e.Cifrar(mensaje, clave));
        } else {
            JOptionPane.showMessageDialog(null, "El mensaje original es: " + d.cifrarAvanzado(mensaje, clave));
        }

    }

}
