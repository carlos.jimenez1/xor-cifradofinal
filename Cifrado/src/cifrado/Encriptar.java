package cifrado;

import java.security.Principal;
import java.util.Arrays;
import javax.swing.JOptionPane;

/**
 *
 * @author Carlos Alberto
 */
public class Encriptar {

    public String Cifrar(String mensaje, String clave) {

        char con = 0;
        String tot = "";
        int ek = 0;
        int i = 0, k, c;

        int[] mEncript = new int[mensaje.length()];
        int[] desEncriptar = new int[mensaje.length()];
        char caracteres[] = new char[mensaje.length()];

        if (clave.length() == 1) {
            ek = (int) clave.charAt(0); //convirtiendo a entero y tomando el primer caracter de k

            for (i = 0; i < mensaje.length(); i++) { //calcular el for 0 a 3
                mEncript[i] = (int) mensaje.charAt(i) ^ ek; // xor guardando los caracteres

            }

            for (k = 0; k < mensaje.length(); k++) { //calcular el for 0 a 3
                desEncriptar[k] = (int) mEncript[k] ^ ek;

                con = caracteres[k] = (char) desEncriptar[k];
                tot += con;
            }

        }
        //System.out.printf("%20s \n", "Mensaje Cifrado");
        //System.out.printf("%20s \n", Arrays.toString(mEncript));

        return tot;
    }

}
