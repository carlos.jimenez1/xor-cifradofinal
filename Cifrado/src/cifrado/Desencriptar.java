/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cifrado;

import java.util.Arrays;
import javax.swing.JOptionPane;

/**
 *
 * @author Carlos Alberto
 */
public class Desencriptar {

    public String cifrarAvanzado(String mensaje, String clave) {

        char con = 0;
        String tot = "";
        int ek = 0;
        int i = 0, k, c;
        int contador = 0, contador2 = 0, contador3 = 0;

        int[] mEncript = new int[mensaje.length()];
        int[] desEncriptar = new int[mensaje.length()];
        char caracteres[] = new char[mensaje.length()];

        for (int j = 0; j < clave.length(); j++) {
            if (contador2 == clave.length()) {
                contador2 = 0;
            }
            ek = (int) clave.charAt(contador2);
            contador2++;
            i = 0;

            for (i = 0; i < mensaje.length(); i++) {
                mEncript[contador] = (int) mensaje.charAt(contador) ^ ek;
                contador++;

                if (j == clave.length() - 1) {
                    j = 0;
                }
                i = mensaje.length();
            }
            if (contador == mensaje.length()) {
                j = clave.length();
            }
        }

//CILCO PARA DESENCRIPTAR
        for (k = 0; k < mensaje.length(); k++) {
            if (contador3 == clave.length()) {
                contador3 = 0;
            }
            ek = (int) clave.charAt(contador3);
            contador3++;
            c = 0;
            for (c = 0; c < clave.length(); c++) {

                desEncriptar[k] = (int) mEncript[k] ^ ek;
                con = caracteres[k] = (char) desEncriptar[k];
                tot += con;
                c = clave.length();

                if (k == mensaje.length() - 1) {
                    k = mensaje.length();
                }
            }
        }

        System.out.printf("%20s \n", "Mensaje cifrado: ");
        System.out.printf("%20s \n", Arrays.toString(mEncript));
        return tot;

    }

}
